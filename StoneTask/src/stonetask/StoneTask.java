package stonetask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StoneTask {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = sc.nextInt();
        }

        int different = find(array);
        if (array.length == 1) {
            System.out.print(array[0]);
        } else {
            System.out.println(different);
        }
    }

    //поиск минимальной разницы
    private static int find(int[] stoneArray) {
        int min = Integer.MAX_VALUE;
        int length = Math.round(stoneArray.length / 2);
        for (int i = 1; i <= length; i++) {
            List<int[]> list = generateArrayIndexes(i, stoneArray.length);
            int listLength = list.size();

            for (int index = 0; index < listLength; index++) {
                int[] firstIndexArray = list.get(index);
                int[] secondIndexArray = reverseIndexArray(firstIndexArray, stoneArray.length);
                int difference = Math.abs(sumStack(stoneArray, firstIndexArray) - sumStack(stoneArray, secondIndexArray));

                if (difference < min) {
                    min = difference;
                } else if (difference == 0) {
                    return 0;
                }
            }
        }
        return min;
    }
    //формирование индексов второй кучи, на основе первой

    private static int[] reverseIndexArray(int[] indexArray, int maxNumber) {
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= maxNumber; i++) {
            if (!contains(i, indexArray)) {
                list.add(i);
            }
        }

        int[] returnArray = new int[list.size()];

        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = list.get(i);
        }

        return returnArray;
    }

    //содержит ли массив число
    private static boolean contains(int index, int[] oldArray) {
        for (int i = 0; i < oldArray.length; i++) {
            if (index == oldArray[i]) {
                return true;
            }
        }
        return false;
    }

    //найти сумму кучи
    private static int sumStack(int[] fullArray, int[] indexArray) {
        int sum = 0;
        for (int i = 0; i < indexArray.length; i++) {
            sum += fullArray[indexArray[i] - 1];
        }
        return sum;
    }
    //генерирует индексы(положение) всех возможных вариантов куч
    private static List<int[]> generateArrayIndexes(int length, int maxNumber) {
        List<int[]> list = new ArrayList<>();
        int[] array = new int[length];
        boolean flag = true;

        for (int i = 1; i <= length; i++) {
            array[i - 1] = i;
        }
        list.add(Arrays.copyOf(array, array.length));
        while (flag) {
            int current = length - 1;

            for (int i = array[length - 1]; i < maxNumber; i++) {
                array[current]++;
                list.add(Arrays.copyOf(array, array.length));
            }

            int max = maxNumber;

            while (true) {
                current--;
                max--;

                if (current == -1) {
                    flag = false;
                    break;
                }
                if (array[current] != max) {
                    array[current]++;
                    for (int i = current + 1; i < array.length; i++) {
                        array[i] = array[i - 1] + 1;
                    }
                    list.add(Arrays.copyOf(array, array.length));
                    break;
                }
            }
        }
        return list;
    }

}
