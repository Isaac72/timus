
package sandrobook;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;




public class SandroBook {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);       
        String spell = sc.nextLine();
        
        
        Map<String, Integer> map = new HashMap<>();
        int size = spell.length();
        int beginCharCount = Math.round(size / 2);
        //поиск и добавлен всех подстрок в строке и их количество
        for(int charCount = beginCharCount; charCount > 0; charCount--){
            for (int i = 0; i < size - charCount + 1; i++){
                String substr = spell.substring(i, i + charCount);
                if (map.containsKey(substr))
                   map.put(substr, map.get(substr)+ 1);
                else
                    map.put(substr, 1);
            }
        }
        int substringLength = 0;
        int count = 0;
        String powerfullSpell = "";
        
        //поиск подстроки, которая больше всего встречалась 
        for (Map.Entry<String,Integer> entry: map.entrySet()){           
            int keyLength = entry.getKey().length();
            int value = entry.getValue();
            if (value > count || (keyLength > substringLength && value == count)){
                powerfullSpell = entry.getKey();
                substringLength = keyLength;
                count = value;
            }  
        }
        
        if (map.isEmpty())
            System.out.println(spell.charAt(0));
        else
            System.out.println(powerfullSpell);
        
    }
}
