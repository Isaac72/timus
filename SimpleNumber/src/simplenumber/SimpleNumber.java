package simplenumber;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SimpleNumber {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = sc.nextInt();
        }

        int max = findMax(array);
        List<Integer> simpleNumber = getSimpleNumbers(max);

        for (int i = 0; i < array.length; i++) {
            System.out.println(simpleNumber.get(array[i] - 1));
        }
    }

    //поиск максимального элемента в массиве
    private static int findMax(int[] array) {
        int max = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    //получить коллекцию простых чисел до arrayLength
    private static List<Integer> getSimpleNumbers(int arrayLength) {
        List<Integer> simpleNumberList = new ArrayList<>();
        simpleNumberList.add(2);
        simpleNumberList.add(3);
        simpleNumberList.add(5);
        simpleNumberList.add(7);
        int number = 11;
        int count = 4;

        while (count < arrayLength) {
            //проверка на базовые делители, чтобы не требовалось пробегать по полному массиву
            if (number % 2 != 0 || number % 3 != 0 || number % 5 != 0 || number % 7 != 0) {
                if (isSimple(number, simpleNumberList)) {
                    simpleNumberList.add(number);
                    count++;
                }
            }

            number++;
        }

        return simpleNumberList;
    }

    //проверка - является ли число простым
    private static boolean isSimple(int number, List<Integer> list) {
        int size = Math.round(list.size() / 2);
        for (int i = 0; i < size; i++) {
            if (number % list.get(i) == 0) {
                return false;
            }
        }

        return true;
    }
}
